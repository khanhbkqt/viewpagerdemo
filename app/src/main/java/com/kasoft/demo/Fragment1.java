package com.kasoft.demo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Khanh Nguyen on 3/15/16.
 */
public class Fragment1 extends Fragment {

    private TextView textView;
    private int position;

    public static Fragment1 newInstance(int position) {
        Bundle args = new Bundle();
        args.putInt("POSITION", position);

        Fragment1 fragment = new Fragment1();
        fragment.setArguments(args);

        return fragment;
    }

    public Fragment1() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        position = args.getInt("POSITION");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_view, container, false);

        textView = (TextView) view.findViewById(R.id.text_view);

        textView.setText("Hello from FRAGMENT " + position);


        return view;
    }
}
